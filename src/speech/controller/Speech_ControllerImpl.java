package speech.controller;

import java.io.IOException;
import speech.model.SpeechModel;
/**
 * 
 *Controller, that implements the control logic. Use of the pattern singleton
 *
 */
//controller with pattern SINGLETON
public class Speech_ControllerImpl implements Speech_Controller {
	//enable the voice recognition
	public void enableVoiceRecognition(){
		model.startRecognition();
	}
	private static Speech_Controller instance;
	private SpeechModel model;
	private Speech_ControllerImpl() throws IOException{

	}
	/**
	 * method for creating the singleton at the first call
	 * @return instance
	 * @throws IOException
	 */
	public static Speech_Controller getInstance()  throws IOException{
		if(instance==null){
			instance=new Speech_ControllerImpl();
		}
		return instance;
	}

	public void setModel(SpeechModel model){
		this.model=model;
	}

}








