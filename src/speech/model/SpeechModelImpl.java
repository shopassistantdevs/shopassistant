package speech.model;


import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

//imports for the real weather forecasts
import com.github.fedy2.weather.YahooWeatherService;
import com.github.fedy2.weather.data.Channel;
import com.github.fedy2.weather.data.Forecast;
import com.github.fedy2.weather.data.unit.DegreeUnit;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import utilities.Brand;
import utilities.DressSize;
import utilities.TypeDress;
import utils.Constants;
import view.client.ClientController;
import view.filterbar.FilterBarController;
import view.speech.SpeechController;


//use of the jsgf grammar, contained in config_speech

//PS: ho lasciato i Sysout per sicurezza, così almeno le stampe si vedono anche su console

// Use of the pattern Model/Controller for SpeechRecognition. 
//It will be connected to the principal view of the project
/**
 * The model implements the business logic.
 * Use of the singleton: it provides a single controller
 */

public class SpeechModelImpl implements SpeechModel{	

	/**
	 * MACRO for the weather forecast
	 * a support for the method recognizeWeather()
	 * I use it for programming with lambdas
	 * @param daycode
	 * @param word
	 */
	public enum Weather {

		MONDAY ("MON", "monday"),
		TUESDAY ("TUE", "tuesday"),
		WEDNESDAY ("WED", "wednesday"),
		THURSDAY ("THU", "thursday"),
		FRIDAY ("FRI", "friday"),
		SATURDAY ("SAT", "saturday"),
		SUNDAY ("SUN", "sunday");

		private final String daycode ;
		private String word;

		private Weather ( final String daycode, String word ){
			this.daycode = daycode ;
			this.word=word;
		}
		public String dayCode (){
			return this.daycode ;
		}
		public String word(){
			return this.word;
		}
	}

	private Configuration configuration;
	private LiveSpeechRecognizer jsgfRecognizer ;
	private static SpeechModel instance;
	private boolean flag;
	private ActionEvent event;


	//variables and constants for the RecognizePrice() method
	private static final int MINIMUM_PRICE=0;
	private static final int MAXIMUM_PRICE=100;
	private static final int MIN_RANGE=20;
	private static final int MAX_RANGE=80;
	private static final int RANGE=10;
	//initial values
	private int min=MINIMUM_PRICE;
	private int high=MAXIMUM_PRICE;

	//istance of the SpeechController, for doing the SpeechOutput 
	private SpeechController sc=SpeechController.getIstance();
	//istance of the FilterBarController, for calling the setters and getters methods that I need
	private FilterBarController types=FilterBarController.getIstance();
	//istance of Client Controller
	private ClientController controller=ClientController.getIstance();

	/**
	 * method for creating the singleton at the first call
	 * @return instance
	 * @throws IOException
	 */
	public static SpeechModel getInstance() throws IOException{
		if(instance==null){
			instance=new SpeechModelImpl();
		}
		return instance;

	}

	/**
	 * private constructor Model(), where all configurations are launched before the start of speechRecognition
	 * @throws IOException
	 */
	private SpeechModelImpl() throws IOException{
		configuration = new Configuration();
		configuration.setAcousticModelPath(Constants.ACOUSTIC_MODEL);
		configuration.setDictionaryPath(Constants.DICTIONARY_PATH);
		configuration.setGrammarPath(Constants.GRAMMAR_PATH);
		configuration.setUseGrammar(true);

		configuration.setGrammarName("dialog");
		jsgfRecognizer =
				new LiveSpeechRecognizer(configuration);

		configuration.setUseGrammar(false);

	}


	void ResetFilters(){
		Platform.runLater(new Runnable() {
			public void run() {
				types.resetFilters();
			}});
	}

	void SetOutput(String output){
		Platform.runLater(new Runnable() {
			public void run() {
				sc.setOutput(output);
			}});
	}

	void ClearOutput(){
		Platform.runLater(new Runnable() {
			public void run() {
				sc.clearOutput();
			}});
	}

	void Search(){
		Platform.runLater(new Runnable(){
			public void run(){
				controller.search(event);
			}});
	}
	void Close(){
		Platform.runLater(new Runnable(){
			public void run(){
				sc.close();
			}});
	}

	void ActivateFilters(){
		Platform.runLater(new Runnable(){
			public void run(){
				types.enableFilters();
			}});
	}

	public void startRecognition(){
		flag=true;
		jsgfRecognizer.startRecognition(true);	

		Runnable task = () -> { 	while (flag) {
			//the button of tooglefilters is enabled after the call of the speech
			ActivateFilters();
			//it resets all the previous filters
			ResetFilters();

			Search();
			//when it goes back to the principal menu the previous output is cancelled
			ClearOutput();

			SetOutput("-----------------------------------------------------------\n");
			SetOutput("Choose menu item:\n");
			SetOutput("-> Shopping\n");
			SetOutput("-> Discount\n");
			SetOutput("-> Weather forecast\n");
			SetOutput("-> Calculator\n");
			SetOutput("-> Exit the program\n");
			SetOutput("If you want to close the window and exit you have to say 'Exit the program'\n");
			SetOutput("-----------------------------------------------------------\n");

			System.out.println("Choose menu item");
			System.out.println("Shopping [with the salesperson]");
			System.out.println("Discount:[new entries]");
			System.out.println("Weather forecast");
			System.out.println("Calculator");
			System.out.println("Exit the program");

			String utterance = jsgfRecognizer.getResult().getHypothesis();

			if (utterance.startsWith("exit")){
				ClearOutput();
				//Recognizer.State status=State.READY;
				SetOutput("Speech Recognition exited");
				stopRecognition();
				Close();
				/*Recognizer.State status=State.DEALLOCATED;
				if(status==State.RECOGNIZING){
					status=State.DEALLOCATED;
				}
				System.out.println(status);*/
				break;
			}

			else if (utterance.equals("discount")) {
				jsgfRecognizer.stopRecognition();
				recognizeDiscount();
				jsgfRecognizer.startRecognition(true);
			}

			else if (utterance.equals("shopping")) {
				jsgfRecognizer.stopRecognition();
				recognizeShopping();
				jsgfRecognizer.startRecognition(true);
			}

			else if (utterance.endsWith("weather forecast")) {
				jsgfRecognizer.stopRecognition();
				recognizeWeather();
				jsgfRecognizer.startRecognition(true);
			}
			else if (utterance.equals("calculator")) {
				jsgfRecognizer.stopRecognition();
				recognizerCalculator();
				jsgfRecognizer.startRecognition(true);
			}
		}
		};

		// start the thread
		new Thread(task).start();
	}

	public void stopRecognition(){
		flag=false;
		jsgfRecognizer.stopRecognition();
	}


	public void recognizeDiscount() {
		//the previous menu is cleared after a choice
		ClearOutput();

		SetOutput("Choose menu item:\n");
		SetOutput("- shop window (items with no brand) (say 'shop')\n");
		SetOutput("- [discounts] in new entries (say 'discounts')\n");
		SetOutput("back (say 'back' if you want to exit)\n");
		SetOutput("--------------------------------------------\n");

		System.out.println("Choose menu item:");
		System.out.println("shop window (say 'shop')");   
		System.out.println("[discounts] in new entries (say 'discounts')");
		System.out.println("back");


		jsgfRecognizer.startRecognition(true);
		String utterance="";
		while (flag) {
			utterance = jsgfRecognizer.getResult().getHypothesis();
			if (utterance.equals("back")){
				//it turns to the principal menu and it clears the previous output
				break;
			}
			else if( utterance.equals("shop")){
				ResetFilters();
				//the previous menu of discount is cleared
				SetOutput("The new entries with no brand:... End with 'back'\n");

				System.out.println("The new entries:... End with 'back'");
				//a vision of the entire shop-> clothes with no brand
				Platform.runLater(new Runnable() {public void run() { types.setChb_filterbrand(Brand.NO_BRAND);}});
				Search();
			}

			else if(utterance.equals("discounts")){
				//it resets previous filters
				ResetFilters();
				SetOutput("New entries in discount: ... End with 'back'\n");

				System.out.println("New entries in discount: ... End with 'back'");
				//all items in discounts
				Platform.runLater(new Runnable() {public void run() { types.setCkb_sale(true);  }});
				Search();
			}
		}
		jsgfRecognizer.stopRecognition();
	}
	/**
	 * a method that is called in recognizeShopping(), where they discuss about the size
	 * Sometimes is hard to recognize 'large' , maybe for the pronunciation
	 * @param string that is a specific item I've chosen
	 * @return void
	 */
	private void recognizeSize(String string) {

		SetOutput("What size/number would you want?\n");
		SetOutput("Choose menu item:\n");
		SetOutput("say 'small' ->(if you want a S size)\n");
		SetOutput("say 'medium' ->(if you want a M size)\n");
		SetOutput("say 'large' ->(if you want a L size)\n");
		SetOutput("say 'extra large' ->(if you want a XL size)\n");
		SetOutput("say 'double extra large' ->(if you want a XXL size)\n");
		SetOutput("say 'Ok' ->(if the item is ok and you want to continue talking with me about it\n"
				+ "\t\t or if you want to return to the principal menu)\n");

		SetOutput("------------------------------------------------------\n");
		System.out.println("What size/number would you want?");
		System.out.println("Choose menu item:");
		System.out.println("say 'small' (if you want a S size)");
		System.out.println("say 'medium' (if you want a M size)");
		System.out.println("say 'large' (if you want a L size");
		System.out.println("say 'extra large' (if you want a XL size");
		System.out.println("say 'double extra large' (if you want a XXL size");
		System.out.println("'Ok' (if the item is ok-> (with ok you continue talking with me about it)\n"
				+ "\t\t\t or if you want to return to the principal menu");


		while (flag) {
			final String utterance = jsgfRecognizer.getResult().getHypothesis();
			List<String> sizes=new ArrayList<>();
			Arrays.asList(DressSize.values()).stream().forEach(x->sizes.add(x.word()));
			if(utterance.endsWith("ok")){
				ClearOutput();
				SetOutput("OK! You continue to talk with me about this\n");

				System.out.println("ok");
				System.out.println("Continue to talk with me about this");
				//with break I go in recognizePrice()
				break;
			}else{
				sizes.stream().forEach(x->{
					if(utterance.startsWith(x)){
						System.out.println(DressSize.getEnum(utterance));

						SetOutput(utterance+"\n");

						Platform.runLater(new Runnable() {public void run() { types.setChb_filtersize(DressSize.getEnum(utterance)); }});
						Search();
					}
				});
			}
		}
	}
	// a private method for the method RecognizePrice()
	private void checkConditions(){
		if (min<MINIMUM_PRICE){
			min=MINIMUM_PRICE;
		}if(high>MAXIMUM_PRICE){
			high=MAXIMUM_PRICE;
		}if(high<MIN_RANGE){
			high=MIN_RANGE;
		}if(min>MAX_RANGE){
			min=MAX_RANGE;
		}
	}
	// a private method for the method RecognizePrice()
	private boolean finish(){
		if(min==MINIMUM_PRICE && high==MIN_RANGE){
			SetOutput("The least limit reached. Stop\n");
			return true;
		}
		if(min==MAX_RANGE && high==MAXIMUM_PRICE){
			SetOutput("The bigger limit reached. Stop\n");
			return true;
		}
		return false;
	}

	/**
	 * a method that is called in recognizeShopping(), when they discuss about the price
	 * @param string that is a specific item I've chosen
	 * @return void
	 */
	private void recognizePrice(String string){
		ClearOutput();
		SetOutput("The price is ok?\n");
		SetOutput("Choose menu item\n");
		SetOutput("say 'cheaper' (if you want to view cheaper clothes of the same type)\n");
		SetOutput("say 'more expensive' (if you want to view more expensive clothes of the same type)\n");
		SetOutput("say 'discount' (if you want to view clothes in discount of the same type)\n");
		SetOutput("say 'yes' (if the item is ok and you want to continue talking with me about it\n "
				+ "\t\tor if you want to return to the principal menu)\n");
		SetOutput("--------------------------------------------------------------------------\n");

		System.out.println("The price is ok?");
		System.out.println("Choose menu item");
		System.out.println("'cheaper' (if you want to view cheaper clothes of the same type)");
		System.out.println("'more expensive' (if you want to view more expensive clothes of the same type)");
		System.out.println("'discount' (if you want to view clothes in discount of the same type)");
		System.out.println("'yes' (if the item is ok-> (with 'yes' you continue to talk with me about it) "
				+ "or if you want to return to the principal menu");

		String utterance= "";
		while (flag) {
			utterance = jsgfRecognizer.getResult().getHypothesis();
			if(utterance.startsWith("cheaper")){

				SetOutput("Cheaper\n");

				finish();
				min=min-RANGE;
				high=high-RANGE;
				//it shows a range for cheaper
				this.checkConditions();

				System.out.println("Cheaper");
				System.out.println("min is:" + min);
				System.out.println("max is:" + high);

				Platform.runLater(new Runnable() {public void run() { types.setSld_pricerange(min, high); }});
				Search();
			}else if(utterance.startsWith("more expensive")){

				SetOutput("more expensive\n");

				finish();
				min=min+RANGE;
				high=high+RANGE;
				//it shows a range for more expensive
				this.checkConditions();

				System.out.println("more expensive");
				System.out.println("min is:" + min);
				System.out.println("max is:" + high);


				Platform.runLater(new Runnable() {public void run() { types.setSld_pricerange(min, high); }});
				Search();

			}else if(utterance.startsWith("discount")){

				SetOutput("in discount\n");

				System.out.println("in discount");
				//it shows discounts in the item you're interested in
				Platform.runLater(new Runnable() {public void run() { types.setCkb_sale(true); }});
				Search();
			}else if(utterance.endsWith("yes")){
				//the price is ok
				ClearOutput();
				SetOutput("YES!Say 'back' if you want to exit or change clothes,instead continue to talk with me about this\n");

				System.out.println("yes");
				System.out.println("Say 'back' if you want to exit or change clothes,instead continue to talk with me about this");
				//with break I go in recognizeLike()
				break;
			}
		}
	}
	/**
	 * a method that is called in recognizeShopping(), when they finish the dialog
	 * @param string that is the specific item I've chosen
	 */

	private void recognizeLike(String string){
		ClearOutput();
		SetOutput("Would you want to try one of them?\n");
		SetOutput(" Say 'ok' or 'no'\n");
		System.out.println("Would you want to try one of them?");
		System.out.println(" Say 'ok' or 'no'");
		String utterance= "";
		while (flag) {
			utterance = jsgfRecognizer.getResult().getHypothesis();
			if(utterance.startsWith("ok")){
				ClearOutput();
				//it resets also price
				min=MINIMUM_PRICE;
				high=MAXIMUM_PRICE;

				SetOutput("The fitting rooms are at the end of the corridor, on the right\n");
				SetOutput("You have to pay at the cash desk, it's at the end of the corridor, on the left\n");
				SetOutput("Say 'back' if you want to view other items or exploring the menu item or exit\n");

				System.out.println("The fitting rooms are at the end of the corridor, on the right");
				System.out.println("You have to pay at the cash desk, they are at the end of the corridor, on the left");
				System.out.println("Say 'back' if you want to view other items or exploring the menu item or exit");
				break;
			}else if(utterance.endsWith("no")){
				ClearOutput();
				//it resets also price
				min=MINIMUM_PRICE;
				high=MAXIMUM_PRICE;

				SetOutput("Say 'back' if you want to view other items or exit\n");

				System.out.println("Say back if you want to view other items or exit");
				//back to the principal menu
				//reset of the previous filters
				ResetFilters();
				Search();
				break;
			}

		}
	}

	public void recognizeShopping() {
		ClearOutput();
		SetOutput("This is a shopping account voice menu\n");
		SetOutput("Hello! What would you want?\n");
		SetOutput("-------------------------------\n");
		SetOutput("JERSEY, UNDERSHIRT, TROUSERS\n");
		SetOutput("JEANS, SKIRT, UNDERWEAR\n");
		SetOutput("JACKET, HAT, SCARF, DRESS\n");
		SetOutput("SHOES, BOOTS, SANDALS, FLIP_FLOP\n");
		SetOutput("-------------------------------\n");
		SetOutput("EXAMPLE: Say 'trousers'\n");
		SetOutput("-------------------------------------\n");

		System.out.println("This is a shopping account voice menu");
		System.out.println("Hello! What would you want?");
		System.out.println("-------------------------------");
		System.out.println("JERSEY, UNDERSHIRT, TROUSERS,");
		System.out.println("JEANS, SKIRT, LONGSKIRT, UNDERWEAR");
		System.out.println("JACKET, HAT, SCARF");
		System.out.println("SHOES, BOOTS, SANDALS, FLIP_FLOP, DRESS");
		System.out.println("-------------------------------");
		System.out.println("EXAMPLE: Say 'trousers'");


		List<String> clothes=new ArrayList<>();
		Arrays.asList(TypeDress.values()).stream().forEach(x->clothes.add(x.word()));

		jsgfRecognizer.startRecognition(true);
		/*String utterance= "";*/
		while (flag) {
			final String utterance = jsgfRecognizer.getResult().getHypothesis();

			if (utterance.endsWith("back")) {
				SetOutput("back\n");
				ClearOutput();
				//back to the principal menu
				break;										
			}else{
				clothes.stream().forEach(x->{
					if(utterance.startsWith(x)){
						ClearOutput();
						SetOutput(utterance+"\n");

						System.out.println(utterance);
						System.out.println(TypeDress.getEnum(utterance));
						//I pass to the view the item I've said
						Platform.runLater(new Runnable() {public void run() { types.setChb_filtertype(TypeDress.getEnum(utterance)); }});
						Search();
						//the dialog continues
						dialog(x);
					}
				});
			}
		}
		jsgfRecognizer.stopRecognition();      


	}
	/**
	 * a private method that allow you to continue the dialog
	 * @param string
	 */

	private void dialog(String string){
		//dialogue about the size
		recognizeSize(string);
		//dialogue about the price
		recognizePrice(string);
		//closing of the dialog
		recognizeLike(string);
	}


	public void recognizerCalculator() {
		ClearOutput();
		SetOutput("This is a ' very simple calculator' voice menu\n");
		SetOutput("-------------------------------\n");
		SetOutput("Example: say 'balance'\n");
		SetOutput("Example: say 'withdraw zero point five'\n");
		SetOutput("Example: say 'deposit one two three'\n");
		SetOutput("->In this case 123 is to pronunciate one two three (the same for other numbers)\n");
		SetOutput("Say 'back' to exit\n");
		SetOutput("-------------------------------\n");

		System.out.println("This is a ' very simple calculator' voice menu");
		System.out.println("-------------------------------");
		System.out.println("Example: balance");
		System.out.println("Example: withdraw zero point five");
		System.out.println("Example: deposit one two three");
		System.out.println("Say 'back' to exit");
		System.out.println("-------------------------------\n");

		double savings = .0;
		jsgfRecognizer.startRecognition(true);
		String utterance="";
		while (flag) {
			utterance = jsgfRecognizer.getResult().getHypothesis();
			if (utterance.endsWith("back")) {
				SetOutput("back");
				//it returns to the principal menu
				break;
			} else if (utterance.startsWith("deposit")) {
				double deposit = parseNumber(utterance.split("\\s"));
				savings += deposit;
				String string = String.valueOf(deposit);
				SetOutput("Deposited:"+ string + "\n");

				System.out.println("Deposited:"+ string + "\n");
				System.out.format("Deposited: $%.2f\n", deposit);
			} else if (utterance.startsWith("withdraw")) {
				double withdraw = parseNumber(utterance.split("\\s"));
				savings -= withdraw;
				String string=String.valueOf(withdraw);
				SetOutput("Withdrawn:"+ string + "\n");

				System.out.println("Withdrawn:"+ string + "\n");
				System.out.format("Withdrawn: $%.2f\n", withdraw);
			} else if (!utterance.endsWith("balance")) {
				System.out.println("Unrecognized command: " + utterance);
			}
			String string=String.valueOf(savings);
			SetOutput("Your Savings: "+ string + "\n");

			System.out.println("Your Savings: "+ string + "\n");
			System.out.format("Your savings: $%.2f\n", savings);
		}

		jsgfRecognizer.stopRecognition();
	}

	public void recognizeWeather() {
		ClearOutput();
		SetOutput("Weather of Cesena! Say a day you are interested in! End with 'back'\n");
		SetOutput("EXAMPLE: say 'tuesday' - it could happen you have to wait (only a few seconds) for the loading\n");

		System.out.println("Say a day you are interested in! End with 'back'");
		Map<String,String> weather=new HashMap<>();
		Arrays.asList(Weather.values()).stream().forEach(x->weather.put(x.word(),x.daycode));
		jsgfRecognizer.startRecognition(true);

		while (flag) {
			final String utterance= jsgfRecognizer.getResult().getHypothesis();
			if (utterance.equals("back"))
				//returns to the principal menu, and it clears previous outputs
				break;
			else{
				weather.keySet().stream().forEach(x->{
					if(utterance.equals(x)){

						String daycode=weather.get(x);
						YahooWeatherService service=null;;
						try {
							service = new YahooWeatherService();
						} catch (JAXBException e) {
							e.printStackTrace();
						}
						Channel channel=null;
						try {
							//the first parameter is the code of the city of Cesena
							channel = service.getForecast("713883", DegreeUnit.CELSIUS);
						} catch (JAXBException | IOException e) {
							e.printStackTrace();
						}
						SetOutput("**************************************************************\n");
						SetOutput(channel.getTitle() + " --- " + channel.getItem().getCondition().getText()+ " today\n" );
						SetOutput("Sunrise: "+ channel.getAstronomy().getSunrise().getHours()+":"+
								channel.getAstronomy().getSunrise().getMinutes()+" AM");
						SetOutput(" --- Sunset: "+channel.getAstronomy().getSunset().getHours()+":"+
								channel.getAstronomy().getSunset().getMinutes()+" PM"+ "\n");
						SetOutput("Humidity: " + channel.getAtmosphere().getHumidity()+"% , " + 
								"Pressure: " + channel.getAtmosphere().getPressure()+"% , "+ 
								"Visibility: " + channel.getAtmosphere().getVisibility()+"%\n");
						SetOutput("Wind: (Chill: " +channel.getWind().getChill()+", Direction: "+channel.getWind().getDirection()
								+", Speed: "+channel.getWind().getSpeed()+")\n");
						SetOutput("--------------------------------------------------------------\n");
						for(Forecast f: channel.getItem().getForecasts()) {
							String format=f.getDay().toString();
							if(format==daycode){
								//if(f.getDay().equals(daycode)){
								DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
								SetOutput("Weather Forecast for the day: " + f.getDay() + " " + df.format(f.getDate()) + "\n");
								SetOutput("-> "+f.getText() + " -- " + "Min : " + f.getLow() + ", Max : " + f.getHigh() + "\n");

								System.out.println(channel.getAstronomy());
								System.out.println(channel.getAtmosphere());
								System.out.println(channel.getLastBuildDate());
								System.out.println("Weather Forecast for the day: " + f.getDay() + " " + df.format(f.getDate()) + "\n");
								System.out.println("-> "+f.getText() + " -- " + "Min : " + f.getLow() + ", Max : " + f.getHigh() + "\n");
							}
						}
					}

				});
			}
		}

		jsgfRecognizer.stopRecognition();
	}
	private static final Map<String, Integer> DIGITS =
			new HashMap<String, Integer>();

	static {
		DIGITS.put("oh", 0);
		DIGITS.put("zero", 0);
		DIGITS.put("one", 1);
		DIGITS.put("two", 2);
		DIGITS.put("three", 3);
		DIGITS.put("four", 4);
		DIGITS.put("five", 5);
		DIGITS.put("six", 6);
		DIGITS.put("seven", 7);
		DIGITS.put("eight", 8);
		DIGITS.put("nine", 9);
	}
	/**
	 * method for using the numbers you say! It is an utility of recognizeCalculator()
	 * */
	private static double parseNumber(String[] tokens) {
		StringBuilder sb = new StringBuilder();

		for (int i = 1; i < tokens.length; ++i) {
			if (tokens[i].equals("point"))
				sb.append(".");
			else
				sb.append(DIGITS.get(tokens[i]));
		}

		return Double.parseDouble(sb.toString());
	}


}
