package speech.model;


public interface SpeechModel {

	/**
	 * Start of the recognition, with the view of the principal menu
	 */
	void startRecognition();

	/**
	 * Stops recognition process until the call of another process
	 */
	void stopRecognition();

	/**
	 * method for viewing a shop window with no brand clothes or discounts
	 * 
	 * @return void
	 */
	void recognizeDiscount();

	/**
	 * it implements a dialog with the salesperson, and it calls other private methods for discussing about the size(recognizeSize()),
	 * the price(recognizePrice()), likes(RecognizeLike())..
	 * ps: avoid uttering T-shirt, scarf ,skirt. They are contained in the SpeechRecognition vocabulary but the speechRecognition difficultly recognizes them
	 * Sometimes it can happen that it recognizes them, but it's more rarely. Maybe it could depends also from the pronunciation
	 * @return void
	 */
	void recognizeShopping();

	/**
	 * A method that implements a calculator that executes, with voice commands, simple operations
	 * It could be useful for example if a customer wants to check if he/she has got enough money to buy what he/she has chosen
	 * This sometimes has some difficulties to recognize the number you want, for the pronunciation
	 * @return void
	 */
	void recognizerCalculator();

	/**
	 * a method that implements a real weather forecast. You can say a day you are interested in for knowing the weather
	 * Sometimes thursay and sunday are hard to be recognized, maybe for my pronunciation
	 * @return void
	 */
	void recognizeWeather();
}
