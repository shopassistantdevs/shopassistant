package view.login;

import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;

import controller.ControllerImpl;
import javafx.fxml.FXML;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import view.shopcreator.ShopCreatorController;
import view.utils.Utils;

public final class LoginController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;
    
    @FXML
    private JFXButton btn_login;
    
    @FXML
    private JFXTextField txt_username;

    @FXML
    private JFXPasswordField txt_password;
    
    @FXML
    private VBox vbox_menu;
    
    private static LoginController istance=null;

    private LoginController() {}

    public static LoginController getIstance() {
        synchronized (LoginController.class) {
            if(istance==null){
                    istance = new LoginController();
            }
        }
        return istance;
    }
    

    ShopCreatorController scc = ShopCreatorController.getIstance();
    ControllerImpl ci = ControllerImpl.getIstance();
    
    @FXML
    void initialize() {
    	view.utils.Utils.loadSidebar(vbox_menu);
    }
    
    @FXML
    void login(MouseEvent event) {
    	
        if(ci.loginMethod(txt_username.getText(), txt_password.getText())) {
            view.utils.Utils.changeFxml(btn_login, "/view/shopcreator/ShopCreator.fxml",scc);
        }
        else {
            Utils.alert(AlertType.WARNING, "WRONG INPUTS", "Attention", "Check your inputs");
        }

    }
}
