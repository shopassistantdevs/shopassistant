package view.shopcreator;

import utilities.ShopElement;

public interface ShopMap {

	/**
	 * this method return the map of shop
	 */
	public ShopElement[][] getShopMap();
	
	/**
	 * this method return angle of all element on the map of shop
	 */
	public double[][] getAngleMap();
	
	/**
	 * this method set element on shop map
	 * @param x
	 * @param y
	 * @param elem
	 */
	public void setElementOnMap(int x, int y, ShopElement elem);
	
	/**
	 * this method set angle of element on shop map
	 * @param x
	 * @param y
	 * @param elem
	 */
	public void setAngleElement(int x, int y, double angle);
	
	/**
	 * this method get element on shop map
	 * @param x
	 * @param y
	 */
	public ShopElement getElementFromMap(int x, int y);

	/**
	 * this method get angle on shop map
	 * @param x
	 * @param y
	 */
	public double getAngleElement(int x, int y);
		

}
