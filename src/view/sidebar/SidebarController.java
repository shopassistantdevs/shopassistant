package view.sidebar;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;
import view.about.AboutController;
import view.client.ClientController;
import view.itemdetails.ItemdetailsController;
import view.login.LoginController;


public class SidebarController {

    @FXML
    private JFXButton btn_admin;
    
    @FXML
    private JFXButton btn_menu;

    @FXML
    private JFXButton btn_settings;

    @FXML
    private JFXButton btn_about;

    @FXML
    private JFXButton btn_quit;
    
    @FXML
    void initialize() {
    }
    
    @FXML
    void exit(MouseEvent event) {
    	System.exit(0);
    }
    
    AboutController ac = AboutController.getIstance();
    LoginController lc = LoginController.getIstance();
    ClientController cc = ClientController.getIstance();
    ItemdetailsController ic = ItemdetailsController.getIstance();

    @FXML
    void openAbout(MouseEvent event) {
    	view.utils.Utils.changeFxml(btn_about,"/view/about/About.fxml",ac);
    }

    @FXML
    void openAdmin(MouseEvent event) {
    	view.utils.Utils.changeFxml(btn_about,"/view/login/Login.fxml",lc);

    }
    
    @FXML
    void openMenu(ActionEvent event) {
    	view.utils.Utils.changeFxml(btn_about,"/view/client/Client.fxml",cc);
    }
    
}
