package view.email;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;

import controller.ControllerImpl;
import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class EmailController {
        
    @FXML
    private JFXTextField txt_recipient;

    @FXML
    private JFXTextField txt_username;

    @FXML
    private JFXPasswordField txt_password;
    
    @FXML
    private JFXComboBox<String> chb_message;

    @FXML
    private JFXButton btn_send;
    
    private ControllerImpl ci = ControllerImpl.getIstance();
    
    private String prod;

    private static EmailController istance=null; //riferimento all' istanza
    
    private EmailController() {}//costruttore

    public static EmailController getIstance() {
        synchronized (EmailController.class) {
            if(istance==null){
                    istance = new EmailController();
            }
        }
        return istance;
    }
    
    @FXML
    void initialize() {
        chb_message.getItems().setAll("Out of stock","Restocked");
        chb_message.setValue("Out of stock");
    }
    
    @FXML
    void SendEmail(MouseEvent event) {
        String[] recipient = {txt_recipient.getText()};
        String message = "";
        String title = "";
        
        switch (chb_message.getSelectionModel().getSelectedIndex()) {
        case 0:
        default:
            title = "Product out of stock";
            message = "The product "+prod+ " is out of stock"; 
            break;
        case 1:
            title = "Product restocked";
            message = "The product "+prod+ " has been restocked";
            break;
        }
        
        ci.SendMail(recipient, title, message, txt_username.getText(), txt_username.getText(), txt_password.getText());
        Stage stage = (Stage) btn_send.getScene().getWindow();
        stage.hide();

    }
    
    
    public void setProd(String prod) {
        this.prod = prod;
    }
    

}
