package view.menu;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBasicCloseTransition;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import view.utils.Utils;

public class MenuController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private JFXHamburger jfx_hamburger;

    @FXML
    private JFXDrawer jfx_drawer;

    @FXML
    void initialize() {
    	
        VBox box;
        try {
            box = FXMLLoader.load(getClass().getResource("/view/sidebar/Sidebar.fxml"));
            jfx_drawer.setSidePane(box);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        
        HamburgerBasicCloseTransition transition = new HamburgerBasicCloseTransition(jfx_hamburger);
        transition.setRate(-1);
        jfx_hamburger.addEventHandler(MouseEvent.MOUSE_PRESSED,(e)->{
            transition.setRate(transition.getRate()*-1);
            transition.play();
            Scene scene = jfx_drawer.getScene();
        	Node root = scene.lookup("#borderpane");
            if(jfx_drawer.isShown()){
            	Utils.playSound("SlideClose.wav");
            	Utils.blurNodes(root, new GaussianBlur(0));
                jfx_drawer.close();
            }
            else{
            	Utils.playSound("SlideOpen.wav");
            	Utils.blurNodes(root, new GaussianBlur(5));
                jfx_drawer.open();
            }});}}
