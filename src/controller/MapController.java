package controller;

import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import model.ShelfImpl;
import utilities.ShopElement;
import view.shopcreator.ShopMapImpl;

public interface MapController {
	
	
	/**
	 * this method draw map of shop on view
	 * @param gridPane
	 * @param grd_shop
	 * @param map
	 */
	public void loadMap(Pane[][] gridPane,GridPane grd_shop, ShopMapImpl map);
	
	/**
	 * this method add a new element at shop map
	 * @param x
	 * @param y
	 * @param elem
	 * @param gridPane
	 * @param map
	 */
	public void  addNewElement(int x, int y, ShopElement elem, Pane[][] gridPane, ShopMapImpl map);
	
	/**
	 * this method draw target shelf on map
	 * @param num
	 * @param gridPane
	 */
	public void drawTargetShelf(int num, Pane[][] gridPane);
	
	
	/**
	 * this method search a specific shelf in the shop
	 * @param num
	 */
	public ShelfImpl searchTargetShelf(int num);
	
	/**
	 * this method set rotation angle of element on map 
	 * @param x
	 * @param y
	 * @param gridPane
	 * @param map
	 */
	public void setAngleOfElement(int x, int y, Pane[][] gridPane, ShopMapImpl map);
	
	/**
	 * this method delete element on map
	 * @param x
	 * @param y
	 * @param gridPane
	 * @param map
	 */
	public void deleteElementOnMap(int x, int y, Pane[][] gridPane, ShopMapImpl map);
	

}
