package controller;

import java.io.File;

import java.io.IOException;

import model.SerializationUtil;
import model.ShopImpl;
import view.shopcreator.ShopMapImpl;

public class SerializerController {
	
		private static final  String SHOP_FILE_NAME = "shop.ser";
		private static final  String SHOP_MAP_NAME = "shopmap.ser";
		private static String SEPARATOR = File.separator;
		private static final String DIRECTORY = System.getProperties().getProperty("user.home") + SEPARATOR +"ShopAssistant";
	
	
	
	
		public static void stampa(){
		
				System.out.println(DIRECTORY);
		
		}
	
	
		/**
		 * this method create a new directory
		 */
		public static void inizializedDirectory(){
		
				File dir = new File(DIRECTORY);
				dir.mkdir();
		
		
		}
	
		/**
		 * this method return path of directory
		 */
		public String getDirectory(){
		
				return DIRECTORY;
		
		}
	
		/**
		 * this method use SerializationUtil.deserialize to load shop to shop.ser
		 * @param shop
		 */
		public static ShopImpl loadShop(ShopImpl shop) {
	
				if(new File(DIRECTORY + SEPARATOR + SHOP_FILE_NAME).exists()){
	   
					   try {						   
						   	shop = (ShopImpl)SerializationUtil.deserialize(DIRECTORY + SEPARATOR + SHOP_FILE_NAME);						   	
					   } catch (ClassNotFoundException e) {						   
						   	e.printStackTrace();					
					   } catch (IOException e) {						 
						   	e.printStackTrace();
					   }
					   
				} else {
		   
					   inizializedDirectory();
					   saveShop(shop);
					   
				}
	   
				return shop;
	
		}

		/**
		 * this method use SerializationUtil.serialize to save shop to shop.ser
		 * @param shop
		 */
		public static void saveShop(ShopImpl shop) {
		
				try {
					SerializationUtil.serialize(shop, DIRECTORY + SEPARATOR + SHOP_FILE_NAME);		
				} catch (IOException e) {
					e.printStackTrace();
				}
		
		}
	
		/**
		 * this method use SerializationUtil.deserialize to load shop map to shopmap.ser
		 * @param map
		 */
		public static ShopMapImpl loadShopMap(ShopMapImpl map) {
		
				if(new File(DIRECTORY + SEPARATOR + SHOP_MAP_NAME).exists()){
			   
		   
					   try {
						   map = (ShopMapImpl)SerializationUtil.deserialize(DIRECTORY + SEPARATOR + SHOP_MAP_NAME);
					   } catch (ClassNotFoundException e) {
						   e.printStackTrace();
					   } catch (IOException e) {
						e.printStackTrace();
					   }
					   
				} else {
			   
					   inizializedDirectory();
					   saveShopMap(map);
					   
				}
		   
		   return map;
		
		}

		/**
		 * this method use SerializationUtil.serialize to save shop map to shopmap.ser
		 * @param shop
		 */
		public static void saveShopMap(ShopMapImpl map) {
			
			try {
				SerializationUtil.serialize(map, DIRECTORY + SEPARATOR + SHOP_MAP_NAME);		
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
}
