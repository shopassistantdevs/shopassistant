package controller;

import java.util.List;

import model.Garment;
import utilities.Brand;
import utilities.DressSize;
import utilities.TypeDress;

public interface SearchController {
	
	/**
	 * this method search one or more garment in the shop garment list that match filter if active
	 * @param list
	 * @param dress 
	 * @param brand
	 * @param size
	 * @param onSale
	 * @param priceMin
	 * @param priceMax
	 */
	 public List<Garment> search(final List<Garment> list, final TypeDress dress, final Brand brand, final DressSize size, final boolean onSale, final double priceMin, final double priceMax );

}
