package controller;


import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import model.Shelf;
import model.ShelfImpl;
import utilities.ShopElement;
import view.shopcreator.ShopMapImpl;




public class MapControllerImpl implements MapController {
	
	
	
	
		private final static int ANGLE_ZERO = 0;
		
		private static MapControllerImpl istance=null; 
		private ControllerImpl ci = ControllerImpl.getIstance();
		
		
		
	    private MapControllerImpl() { }
	
	    
	    public static MapControllerImpl getIstance() {
	    	
	    		synchronized (MapControllerImpl.class) {
	            
	    				if(istance==null){
	                   
	    						istance = new MapControllerImpl();
	    				}
	    		}
	        
	    		return istance;
	        
	    }
		
		
		public void loadMap(Pane[][] gridPane,GridPane grd_shop, ShopMapImpl map){
			
	    			
	    			for(int i = 0; i < 10; i++ ){
	    			
	    					for(int j = 0; j < 8; j++){
	            	
				        		    Pane pane = new Pane();	
				    				grd_shop.add(pane, i, j);
				    				gridPane[i][j] = pane;
				    				this.addElement(i, j, gridPane, map.getElementFromMap(i, j));
				    				gridPane[i][j].setRotate(map.getAngleElement(i, j));
				    				gridPane[i][j].setVisible(true);
	    					
	    					}
	    			}	
		}
	
		
		public void  addNewElement(int x, int y, ShopElement elem, Pane[][] gridPane, ShopMapImpl map){
				
			
				gridPane[x][y].getChildren().clear();
				map.setElementOnMap(x, y, elem);
				this.addElement(x, y, gridPane, elem);
	    	 
				if(elem == ShopElement.SHELF){
	    		
						ci.createNewShelf(x, y);
	    	 
				}    				
		}
	
		
		public void drawTargetShelf(int num, Pane[][] gridPane){
			
				ShelfImpl shelf = searchTargetShelf(num);	
				
				gridPane[shelf.getPosXShelf()][shelf.getPosYShelf()].getChildren().clear();
				gridPane[shelf.getPosXShelf()][shelf.getPosYShelf()].getChildren().add(new ImageView(new Image("shelf_target.png")));
						
		}
		
		
		public ShelfImpl searchTargetShelf(int num){
		
				ShelfImpl targetShelf = new ShelfImpl(0,0,0);
			
				for(Shelf shelf : ci.getShelfList()){
				
						if(shelf.getNShelf() == num){
					
								targetShelf.setNShelf(shelf.getNShelf());
								targetShelf.setPosShelf(shelf.getPosXShelf(), shelf.getPosYShelf());
				
						}
				}
			
				return targetShelf;
		}
		
		
	    public void setAngleOfElement(int x, int y, Pane[][] gridPane, ShopMapImpl map){
	    	
		    	gridPane[x][y].setRotate(map.getShopMap()[x][y].getAngle());
		    	map.setAngleElement(x, y, map.getShopMap()[x][y].getAngle());	    	
	    	
	    }
	
	
	    public void deleteElementOnMap(int x, int y, Pane[][] gridPane, ShopMapImpl map){
	    	    
	    	
	    		if(map.getElementFromMap(x, y) == ShopElement.SHELF){
	    			
	    			ci.deleteShelf(x, y);
	    		
	    		}
	    		
		    	gridPane[x][y].getChildren().clear();
		    	gridPane[x][y].setVisible(false);
		    	map.setElementOnMap(x, y, ShopElement.NONE);
		    	map.setAngleElement(x, y, ANGLE_ZERO);
	    	
	    }
	    
	    
	    private void addElement(int x, int y, Pane[][] gridPane, ShopElement elem){
    	
				switch(elem ){
				
				case  SHELF:
										
						gridPane[x][y].getChildren().add(new ImageView(new Image("shelf.png")));			
						break;
					
				case YOU_ARE_HERE:
								
						gridPane[x][y].getChildren().add(new ImageView(new Image("URHere.png")));			
						break;
					
				case DOOR:
									
						gridPane[x][y].getChildren().add(new ImageView(new Image("door.png")));			
						break;
					
				case PC:
							
						gridPane[x][y].getChildren().add(new ImageView(new Image("pc.png")));			
						break;
					
				case CASH:
							
						gridPane[x][y].getChildren().add(new ImageView(new Image("mobilia.jpg")));			
						break;
				
				default:
										
						gridPane[x][y].getChildren().clear();
															
				}
    	
    }

}
