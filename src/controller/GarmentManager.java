package controller;


import java.util.Iterator;
import model.Garment;
import model.ShopImpl;
import utilities.Brand;
import utilities.DressSize;
import utilities.TypeDress;

public class GarmentManager {
	
	
		/**
		 * this method create new garment,
		 * @param name 
		 * @param price
		 * @param brand
		 * @param discount
		 * @param shelfN
		 * @param type
		 * @param URI
		 * @param size
		 * @param shop
		 */
		static void addGarmentToShop(String name, double price, Brand brand, boolean discount, int shelfN, TypeDress type,String URI, DressSize size, ShopImpl shop){
			
				shop.insertNewGarment(name, price, brand, discount, shelfN, type, URI, size, shop.getCodeCounter().getCode());
			
		}
		
		/**
		 * this method modify garment
		 * @param shop
		 * @param name 
		 * @param price
		 * @param brand
		 * @param discount
		 * @param shelfN
		 * @param type
		 * @param URI
		 * @param size
		 * @param code
		 */
		static void modifyGarmentToShop(ShopImpl shop, String name, double price, Brand brand, boolean discount, int shelfN, TypeDress type,String uri, DressSize size, long code){
			
			shop.modifyGarment(name, price, brand, discount, shelfN, type, uri, size, code);
   
		}
		
		/**
		 * this method delete garment,
		 * @param code 
		 * @param shop
		 */
		static void deleteGarmentToShop(long code, ShopImpl shop){
			
				for (Iterator<Garment> iterator = shop.getListofGarment().iterator(); iterator.hasNext(); ) {
			    
						Garment garment = iterator.next();
						
						if (garment.getCodeNum() == code) {
			       
								iterator.remove();
			    
						}								
				}
		}
	

}
