package controller;


import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;

import model.Garment;
import model.Login;
import model.MailSender;
import model.Shelf;
import utilities.Brand;
import utilities.DressSize;
import utilities.TypeDress;
import view.shopcreator.UsrGListImpl;
import view.shopcreator.ShopCreatorController;
import model.ShopImpl;




public class ControllerImpl implements Controller{

	
		private SearchControllerImpl sci = SearchControllerImpl.getIstance();
		private ShopImpl shop = ShopImpl.getIstance();	
		private static ControllerImpl istance=null; 



	    private ControllerImpl() {}

	    
	    
	    public static ControllerImpl getIstance() {
	    	
	    		synchronized (ControllerImpl.class) {
	            
	    				if(istance==null){
	            	
	    						istance = new ControllerImpl();
	    				}
	    		}
	    		
	    		return istance;
	    }	  

	    
	    public List<Shelf> getShelfList(){
	    	
	    		return this.shop.getListofShelfs();
	    	
	    }
	    
	    
	    public void createNewShelf(int x, int y){
	    	
	    		this.shop.addNewShelf(x, y);
	    	
	    }
	    
	    public void deleteShelf(int x, int y){
	    	
	    	this.shop.deleteShelf(x, y);
	    	
	    }
	    
	    
		@Override
		public List<Garment> getGarmentList() {
			
				return this.shop.getListofGarment();
			
		}
		
				
		@Override
		public void createNewGarment(String name, double price, Brand brand, boolean discount, int shelfN, TypeDress type,String uri, DressSize size) {
			
				GarmentManager.addGarmentToShop(name, price, brand, discount, shelfN, type, uri, size, this.shop);
				refreshViewAndSave();
			
		}
		
		
		@Override
		public void deleteGarmentToShop(long code) {
			
				GarmentManager.deleteGarmentToShop(code, this.shop);
				refreshViewAndSave();
					
		}
		
		
		public void modifyGarmentToshop(String name, double price, Brand brand, boolean discount, int shelfN, TypeDress type,String uri, DressSize size, long code){
			
				GarmentManager.modifyGarmentToShop(this.shop, name, price, brand, discount, shelfN, type, uri, size, code);
				refreshViewAndSave();
		}
				
	    
		@Override
		public List<Garment> multiSearch(TypeDress dress, Brand brand, DressSize size, boolean onSale, double priceMin, double priceMax ) {
		
				return sci.search(this.shop.getListofGarment(), dress, brand, size, onSale, priceMin, priceMax);
	
		}
	

		@Override
		public boolean loginMethod(String username, String password) {
		
				Login login = Login.getIstance();
		  	    return login.match(username, password);	
			
		}
		
        public void SendMail(String recipients[ ], String subject, String message , String from, String username, String password) {
        
            MailSender mail = new MailSender();
            try {
                mail.sendMail(recipients, subject, message, from, username, password);
            } catch (MessagingException e) {
                e.printStackTrace();
            }
            
        }
	
	
		public void saveShop() {
		
				SerializerController.saveShop(this.shop);
		
		}
	   
	
		@Override
		public void loadShop() throws ClassNotFoundException, IOException {

			this.shop = SerializerController.loadShop(this.shop);
	
	    }
		
		private void refreshViewAndSave(){
			
			this.saveShop();
			ShopCreatorController shopCreator = ShopCreatorController.getIstance();
			UsrGListImpl flowPane = UsrGListImpl.getIstance();
			flowPane.garmentListView(this.shop.getListofGarment(), shopCreator.getFlp_list());
			
		}

	
}
