package controller;


import java.io.IOException;
import java.util.List;

import model.Garment;
import model.Shelf;
import utilities.Brand;
import utilities.DressSize;
import utilities.TypeDress;

public interface Controller {
	
	/**
	 * this method is used to get the list of shelf in the shop
	 */
	public List<Shelf> getShelfList();
	
	/**
	 * this method create a new shelf in the shop
	 * @param x
	 * @param y
	 */
	public void createNewShelf(int x, int y);
	
	/**
	 * this method get the list of garment in the shop
	 */
	public List<Garment> getGarmentList();
	
	/**
	 * this method delete shelf
	 * @param x
	 * @param y
	 */
	public void deleteShelf(int x, int y);
	
	/**
	 * this method is used to create new garment,
	 * invoke static method GarmentManager.addGarmentToShop
	 * @param name 
	 * @param price
	 * @param brand
	 * @param discount
	 * @param shelfN
	 * @param type
	 * @param URI
	 * @param size
	 */
	public void createNewGarment(String name, double price, Brand brand, boolean discount, int shelfN, TypeDress type, String URI, DressSize size);
	
	/**
	 * this method is used to delete garment,
	 * invoke GarmentManager.deleteGarmentToShop
	 * @param code
	 */
	public void deleteGarmentToShop(long code);
	
	/**
	 * this method is used to modify existing garment,
	 * invoke static method GarmentManager.modifyGarmentToShop
	 * @param name 
	 * @param price
	 * @param brand
	 * @param discount
	 * @param shelfN
	 * @param type
	 * @param URI
	 * @param size
	 * @param code
	 */
	public void modifyGarmentToshop(String name, double price, Brand brand, boolean discount, int shelfN, TypeDress type,String uri, DressSize size, long code);
	
	/**
     * this method looks for clothes that match the filters
	 * invoke method search to the class SearchControllerImpl
	 * @param dress 
	 * @param brand
	 * @param size
	 * @param onSale
	 * @param priceMin
	 * @param priceMax
	 */
	public List<Garment> multiSearch(TypeDress dress, Brand brand, DressSize size, boolean onSale, double priceMin, double priceMax);
	
	
	/**
	 * this method invoke method match of Shop for identify user identity
	 * @param username
	 * @param password
	 */
	public boolean loginMethod(final String username, final String password);	
	
	/**
     * this method send mail
	 * @param recipients[]
	 * @param subject
	 * @param message
	 * @param from
	 * @param username
	 * @param password
	 */
	public void SendMail(String recipients[ ], String subject, String message , String from, String username, String password);
	
	/**
	 * this method save shop, invoke SerializerController.saveShop
	 */
	public void saveShop();
	
	/**
	 * this method save shop, invoke SerializerController.loadShop
	 * @throws ClassNotFoundException, IOException
	 */
	public void loadShop() throws ClassNotFoundException, IOException;
}
