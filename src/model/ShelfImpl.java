package model;

import java.io.Serializable;

public class ShelfImpl implements Shelf, Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int progressiveNum;
	private int posX;
	private int posY;
	
	public ShelfImpl(int n, int pX, int pY){
		this.progressiveNum=n;
		this.posX= pX;
		this.posY= pY;
	}

	@Override
	public int getNShelf() {
		// TODO Auto-generated method stub
		return progressiveNum;
	}

	@Override
	public void setNShelf(int n) {
		// TODO Auto-generated method stub
		this.progressiveNum=n;
	}

	@Override
	public int getPosXShelf() {
		// TODO Auto-generated method stub
		return this.posX;
	}

	@Override
	public int getPosYShelf() {
		// TODO Auto-generated method stub
		return this.posY;
	}
	
	@Override
	public void setPosShelf(int nX, int nY) {
		// TODO Auto-generated method stub
		this.posX=nX;
		this.posY=nY;
	}

}
