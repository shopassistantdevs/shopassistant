package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import utilities.Brand;
import utilities.DressSize;
import utilities.TypeDress;

public class ShopImpl implements Shop, Serializable{
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Shelf> list = new ArrayList<>();
	private List<Garment> garmentList = new ArrayList<>();
	private static ShopImpl istance=null; //riferimento all' istanza
	private IdCodeCounterImpl dressCode;
	private ShelfCodeCounterImpl shelfCode;
	
	private ShopImpl() {
		dressCode=new IdCodeCounterImpl();
		this.shelfCode=new ShelfCodeCounterImpl();
	}
	

    public static ShopImpl getIstance() {
        synchronized (ShopImpl.class) {
            if(istance==null){
                    istance = new ShopImpl();
            }
        }
        return istance;
    }
	
	@Override
    public void insertNewGarment(String name, double price, Brand brand, boolean discount, int shelfN, TypeDress type, String URI, DressSize size, long codeNum) {
		// TODO Auto-generated method stub
		Garment g = new GarmentImpl(name, price, brand, discount, shelfN, type, URI, size, codeNum);
    		this.garmentList.add(g);
    }
    
	@Override
    public void deleteGarment(final int num) {
		// TODO Auto-generated method stub
    		garmentList.remove(num);
    }
    
	@Override
    public void modifyGarment(String name, double price, Brand brand, boolean discount, Integer shelfN, TypeDress type, String URI, DressSize size, long code) {
		// TODO Auto-generated method stub
		garmentList.forEach(e->{
			if(e.getCodeNum() == code) {
				e.setName(name);
				e.setPrice(price);
				e.setBrand(brand);
				e.setDiscount(discount);
				e.setShelfNum(shelfN);
				e.setType(type);
				e.setPhoto(URI);
				e.setSize(size);
			}
		});
    }
    
	@Override
    public List<Garment> getListofGarment(){
		// TODO Auto-generated method stub
    		return this.garmentList;
    }
    
	@Override
    public void addNewShelf(int pX, int pY) {
		// TODO Auto-generated method stub
    		Shelf s = new ShelfImpl(this.shelfCode.getCode(), pX, pY);
    		this.list.add(s);
    }
    
	@Override
    public void deleteShelf(final int x, final int y) {
		// TODO Auto-generated method stub
    		for (Iterator<Shelf> iterator = this.list.iterator(); iterator.hasNext(); ) { 
			Shelf shelf = iterator.next();
			if (shelf.getPosXShelf() == x && shelf.getPosYShelf() == y) {
				iterator.remove();
			}			
		}
    		
    }
    
	@Override
    public void modifyShelf(final int num, int nX, int nY, int n) {
		// TODO Auto-generated method stub
    		this.list.get(num).setPosShelf(nX, nY);
    		this.list.get(num).setNShelf(n);
    }
   
	@Override
	public List<Shelf> getListofShelfs() { //La lista degli scaffali equivale automaticamente alla mappa del negozio
		// TODO Auto-generated method stub
		return this.list;
	}
	
	@Override
	public int getPositionX(ShelfImpl s){
		// TODO Auto-generated method stub
		return s.getPosXShelf();
	}
	
	@Override
	public int getPositionY(ShelfImpl s){
		// TODO Auto-generated method stub
		return s.getPosYShelf();
	}
	
	@Override
	public IdCodeCounterImpl getCodeCounter() {
		// TODO Auto-generated method stub
		return this.dressCode;
	}
	
	@Override
	public ShelfCodeCounterImpl getShelfCodeCounter() {
		// TODO Auto-generated method stub
		return this.shelfCode;
	}
	
}
