package model;

public interface ShelfCodeCounter {
	
	/**
	 * this method is used to get the progressive code of a field
	 * @return current code
	 */
	
	 int getCode();

	 /**
	  * this method increase of 1 unit the progressive code
	  */
	 
	 void nextCode();

}
