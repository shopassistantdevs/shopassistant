package model;

import java.io.Serializable;
import utilities.Brand;
import utilities.DressSize;
import utilities.TypeDress;

public class GarmentImpl implements Garment, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private double price;
	private Brand brand;
	private boolean discount;
	private int shelfN;
	private TypeDress type;
	private 	DressSize size;
	private String URI;
	private long codeNum;
	
	public GarmentImpl(String name, double price, Brand brand, boolean discount, int shelfN, TypeDress type, String URI, DressSize size, long codeNum) {
		// TODO Auto-generated constructor stub
		super();
		this.name=name;
		this.price=price;
		this.brand=brand;
		this.discount=discount;
		this.shelfN=shelfN;
		this.type=type;
		this.URI=URI;
		this.size=size;
		this.codeNum = codeNum;
	}
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}
	
	@Override
	public String getPhoto() {
		// TODO Auto-generated method stub
		return this.URI;
	}
	
	@Override
	public TypeDress getTypeDress() {
		// TODO Auto-generated method stub
		return this.type;
	}
	
	@Override
	public double getPrice() {
		// TODO Auto-generated method stub
		return price;
	}
	
	@Override
	public Brand getBrand() {
		// TODO Auto-generated method stub
		return brand;
	}
	
	public DressSize getSize() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public Boolean getDiscount() {
		// TODO Auto-generated method stub
		return discount;
	}
	
	@Override
	public int getShelfNum() {
		// TODO Auto-generated method stub
		return this.shelfN;
	}
	
	public long getCodeNum(){
		// TODO Auto-generated method stub
		return this.codeNum;
	}

	public void setName(String name){
		// TODO Auto-generated method stub
		this.name = name;
	}
	
	@Override
	public void setPhoto(String path) {
		// TODO Auto-generated method stub
		this.URI=path;
	}
	
	public void setType(TypeDress dress){
		// TODO Auto-generated method stub
		this.type = dress;
	}
	
	@Override
	public void setPrice(Double fPrice) {
		// TODO Auto-generated method stub
		this.price=fPrice;
	}
	
	public void setBrand(Brand brand){
		// TODO Auto-generated method stub
		this.brand = brand;
	}
	
	public void setSize(DressSize size){
		// TODO Auto-generated method stub
		this.size = size;
	}
	
	@Override
	public void setDiscount(Boolean b) {
		// TODO Auto-generated method stub
		this.discount=b;
	}

	@Override
	public void setShelfNum(Integer nShelf) {
		// TODO Auto-generated method stub
		this.shelfN=nShelf;
	}

}
