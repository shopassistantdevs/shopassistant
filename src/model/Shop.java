package model;

import java.util.List;

import utilities.Brand;
import utilities.DressSize;
import utilities.TypeDress;

public interface Shop {
	
	/**
	 * this method is used to create a new garment and add itself to the list of garments
	 * @param name
	 * @param price
	 * @param brand
	 * @param discount
	 * @param shelfN
	 * @param type
	 * @param URI
	 * @param size
	 * @param codeNum
	 */
	
    void insertNewGarment(String name, double price, Brand brand, boolean discount, int shelfN, TypeDress type, String URI, DressSize size, long codeNum);
		
    /**
     * this method is used to delete a garment searching it from the list of garments by code
     * @param num
     */
    void deleteGarment(final int num);
    
    /**
     * this method is used to modify the fields of a specific garment
     * @param name
     * @param price
     * @param brand
     * @param discount
     * @param shelfN
     * @param type
     * @param URI
     * @param size
     * @param code
     */
    
    void modifyGarment (String name, double price, Brand brand, boolean discount, Integer shelfN, TypeDress type, String URI, DressSize size, long code);
    
    /**
     * this method is used to get the list of garments in the shop
     * @return garmentList
     */
    List<Garment> getListofGarment();
    
    /**
     * this method add a new shelf to the shop
     * @param pX
     * @param pY
     */
    
    void addNewShelf(int pX, int pY);
    
    /**
     * this method delete a shelf searching it from the list of shelfs by position (x, y)
     * @param x
     * @param y
     */
    void deleteShelf(final int x, final int y);
    
    /**
     * this method is used to modify the fields of a specific shelf
     * @param num
     * @param nX
     * @param nY
     * @param n
     */
    void modifyShelf(final int num, int nX, int nY, int n);
    
    /**
     * this method is used to get the list of shelfs in the shop
     * @return list
     */
    
	List<Shelf> getListofShelfs();
	
	/**
	 * this method return the X position of a specific shelf
	 * @param s
	 * @return posX
	 */
	int getPositionX(ShelfImpl s);
	
	/**
	 * this method return the Y position of a specific shelf
	 * @param s
	 * @return posY
	 */
	int getPositionY(ShelfImpl s);
	
	/**
	 * this method is used to get the progressive code of a garment
	 * @return dressCode
	 */
	IdCodeCounterImpl getCodeCounter();
	
	/**
	 * this method is used to get the progressive code of a shelf
	 * @return shelfCode
	 */
	ShelfCodeCounterImpl getShelfCodeCounter();

}
