package model;

import java.io.Serializable;

public class IdCodeCounterImpl implements IdCodeCounter, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long codeNum;
	
	public IdCodeCounterImpl() {
		// TODO Auto-generated constructor stub
	    	this.codeNum=0;
	}

	@Override
	public long getCode() {	
		// TODO Auto-generated method stub
		nextCode();
		return this.codeNum;		
	}
	
	@Override
	public void nextCode() {
		// TODO Auto-generated method stub
		this.codeNum++;
	}
	
}
