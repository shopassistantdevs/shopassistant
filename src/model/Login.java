package model;

public class Login {
	
	private final String ADMINUSR = "admin";
	private final String ADMINPWD = "password";
	private static Login istance=null;
	
	private Login() {
		
	}
	
	/**
	 *  this method return the instance of this class for singleton use (one only instance of this class)
	 * @return
	 */
	public static Login getIstance() {
        synchronized (ShopImpl.class) {
            if(istance==null){
                    istance = new Login();
            }
        }
        return istance;
    }
	
	/**
	 * this method match usr & pwd with default ones
	 * @param usr
	 * @param pwd
	 * @return true if login ok or false if login fail
	 */
	public boolean match(String usr, String pwd) {
		if(this.ADMINUSR.equals(usr) && this.ADMINPWD.equals(pwd)) {
			return true;
		}
		return false;
	}

}
