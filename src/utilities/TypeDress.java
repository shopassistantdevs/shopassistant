package utilities;

import java.util.HashMap;
import java.util.Map;

public enum TypeDress { 
	
	JERSEY ("jersey"), TSHIRT ("tshirt"), UNDERSHIRT("undershirt"), TROUSERS("trousers"),
	JEANS("jeans"), SKIRT("skirt"), DRESS("dress"), 
	JACKET("jacket"), UNDERWEAR("underwear"), HAT("hat"), SHOES("shoes"), 
	BOOTS("boots"), SANDALS("sandals"), FLIP_FLOP("flip flop"),
	SCARF("scarf"), ANY(" ");
	
	private String word;
	
	TypeDress(String word){
		this.word = word;
	}

	public String word(){
		
		return word;
		
	}
	
	private static Map<String, TypeDress> map = new HashMap<>();

    
	static {
		for(TypeDress td : TypeDress.values()){
    	
    		map.put(td.word, td);
    	}

	}
	
	public static TypeDress getEnum(String word){
		
		return map.get(word);
		
	}
	
	
	
}
